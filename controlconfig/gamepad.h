#ifndef PS2GAMEPAD_H
#define PS2GAMEPAD_H

#pragma once

#pragma comment(lib, "Dinput8.lib")
//#pragma comment(lib, "user32.lib")
#pragma comment(lib, "Dxguid.lib")

#define DIRECTINPUT_VERSION 0x0800
#include <vector>
#include "dinput.h"
#include "dbt.h"
#include <InitGuid.h>

struct Gamepad {
	LPDIRECTINPUTDEVICE8 device;
	float left_x, left_y, right_x, right_y;
	float d_left_x, d_left_y, d_right_x, d_right_y;
	bool button[16];
	int dbutton[16];

	enum Button_change { DOWN = -1, NONE = 0, UP = 1 };

	Gamepad();
	Gamepad(LPDIRECTINPUTDEVICE8 device);
	~Gamepad();

	void update();
	void release();

private:
	float Gamepad::normalise(LONG rawValue, bool invert = false);
};

namespace Input {
	extern std::vector<Gamepad> gamepad;
	extern LPDIRECTINPUT8 directinput;
	extern HDEVNOTIFY notifier;
	bool refresh_devices(HWND hWnd);
	bool init(HINSTANCE hinstance, HWND hwnd);
	void shutdown();
	BOOL CALLBACK enumerate_gamepads(const DIDEVICEINSTANCE* device_instance, VOID* vec_gamepad_ptr);

	void set_devicebroadcast(HWND hwnd);
	void reset_devicebroadcast();
}

#endif