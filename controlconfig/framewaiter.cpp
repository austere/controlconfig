#include "stdafx.h"
#include "framewaiter.h"

namespace Timer {
	UINT resolution;

	std::set<HANDLE> timer_handles;
	HANDLE queue_handle = NULL;

	//don't forget to link to winmm.lib :)
	int set_resolution(UINT res) {
		TIMECAPS tc;
		if (timeGetDevCaps(&tc, sizeof(TIMECAPS)) != TIMERR_NOERROR) {
			printf("timerGetDevCaps() failed.\n");
			return -1;
		}

		resolution = min(max(tc.wPeriodMin, res), tc.wPeriodMax);
		printf("Timer resolution: %d ms\n", resolution);
		timeBeginPeriod(resolution);

		return resolution;
	}
	
	void reset_resolution() {
		timeEndPeriod(resolution);
	}

	HANDLE create_timer(WAITORTIMERCALLBACK callback, PVOID param, int delay, int period) {
		HANDLE handle;
		if (!queue_handle)
			queue_handle = CreateTimerQueue();
		CreateTimerQueueTimer(&handle, queue_handle, callback, param, delay, period, 0);
		timer_handles.insert(handle);
		return handle;
	}
}

//=============================================================================
//Frame_waiter::
//=============================================================================
Frame_waiter::Frame_waiter(int frame_rate) : frame_rate(frame_rate) {
	init();
}

void Frame_waiter::init () {
		QueryPerformanceFrequency(&cpu_frequency);
		tick_wait.QuadPart = cpu_frequency.QuadPart / frame_rate;
		low_tick_threshold = (int)(cpu_frequency.QuadPart*2/1000);
		started = false;
}

void Frame_waiter::start() {
	LARGE_INTEGER tick_cur;
	QueryPerformanceCounter(&tick_cur);
	tick_target.QuadPart = tick_cur.QuadPart + tick_wait.QuadPart;
	started = true;
}

//Wait for a full frame
void Frame_waiter::wait() {
	LARGE_INTEGER tick_cur;
	QueryPerformanceCounter(&tick_cur);
	if (!started) {
		tick_target.QuadPart = tick_cur.QuadPart + tick_wait.QuadPart;
		started = true;
	}

	for(;;) {
		//TODO: handle time wrap, see http://www.geisswerks.com/ryan/FAQS/timing.html
		if (tick_target.QuadPart < tick_cur.QuadPart)
			break;

		int tick_left = (int)(tick_target.QuadPart - tick_cur.QuadPart);
		if (tick_left > low_tick_threshold) {
			Sleep(1);
		}
		else {
			for (int i=0; i<10; i++) 
				Sleep(0);
		}
			
		QueryPerformanceCounter(&tick_cur);
	}
	tick_target.QuadPart += tick_wait.QuadPart;
	//Did we miss a whole bunch of frames?
	if (tick_target.QuadPart < tick_cur.QuadPart) {
		tick_target.QuadPart = tick_cur.QuadPart + tick_wait.QuadPart;
	}
}

//Wait a little bit, return true when frame done
bool Frame_waiter::wait_partial() {
	LARGE_INTEGER tick_cur;
	QueryPerformanceCounter(&tick_cur);
	if (!started) {
		tick_target.QuadPart = tick_cur.QuadPart + tick_wait.QuadPart;
		started = true;
	}

	//TODO: handle time wrap
	if (tick_target.QuadPart < tick_cur.QuadPart) {
		tick_target.QuadPart += tick_wait.QuadPart;
		//Did we miss a whole bunch of frames?
		if (tick_target.QuadPart < tick_cur.QuadPart) {
			tick_target.QuadPart = tick_cur.QuadPart + tick_wait.QuadPart;
		}
		return true;
	}
	else {
		int tick_left = (int)(tick_target.QuadPart - tick_cur.QuadPart);
		if (tick_left > low_tick_threshold) {
			Sleep(1);
		}
		else {
			for (int i=0; i<10; i++) 
				Sleep(0);
		}
		return false;
	}

}