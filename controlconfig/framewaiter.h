#ifndef FRAMEWAITER_H
#define FRAMEWAITER_H

#pragma once
#pragma comment(lib, "winmm.lib")

#include <windows.h>
#include <mmsystem.h>
#include <set>

//Could convert into an object...
namespace Timer {
	//don't forget to link to winmm.lib :)
	int set_resolution(UINT res);
	void reset_resolution();
	HANDLE create_timer(WAITORTIMERCALLBACK callback, PVOID param, int delay, int period);
}

//The very epitome of frame waiting.
struct Frame_waiter {
	LARGE_INTEGER cpu_frequency;
	LARGE_INTEGER tick_wait;
	LARGE_INTEGER tick_target;
	int low_tick_threshold;
	int frame_rate;
	bool started;

	Frame_waiter(int frame_rate = 60);

	void init ();

	void start();

	//Wait for a full frame
	void wait();

	//Wait a little bit, return true when frame done
	bool wait_partial();
};

#endif