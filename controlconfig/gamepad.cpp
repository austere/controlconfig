#include "stdafx.h"
#include "gamepad.h"
#include <algorithm>
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
//#include "usbiodef.h"

using namespace std;

vector<Gamepad> Input::gamepad;
LPDIRECTINPUT8 Input::directinput = NULL;
HDEVNOTIFY Input::notifier;

Gamepad::Gamepad(LPDIRECTINPUTDEVICE8 device) : device(device) {}
Gamepad::Gamepad() : device(NULL) {}

Gamepad::~Gamepad() {
}

void Gamepad::release() {
	if (device) {
		device->Unacquire();
		device->Release();
		device = NULL;
	}
}

void Gamepad::update() {
	HRESULT hr;
	hr = device->Poll();
	if (hr != DI_OK) {
		hr = device->Acquire();
		while (hr == DIERR_INPUTLOST)
			hr = device->Acquire();
		device->Poll();
	}

	DIJOYSTATE state;

	hr = device->GetDeviceState(sizeof(state), &state);
	if (hr == DI_OK) {
		auto new_left_x = normalise(state.lX);
		auto new_left_y = normalise(state.lY, true);
		auto new_right_x = normalise(state.lZ);
		auto new_right_y = normalise(state.lRz, true);
		d_left_x = new_left_x - left_x;
		d_left_y = new_left_y - left_y;
		d_right_x = new_right_x - right_x;
		d_right_y = new_right_y - right_y;
		left_x = new_left_x;
		left_y = new_left_y;
		right_x = new_right_x;
		right_y = new_right_y;

		for (int i = 0; i<16; i++) {
			bool cur_button = (state.rgbButtons[i] != 0) ? true : false;
			if (cur_button ^ button[i])
				dbutton[i] = cur_button ? DOWN : UP;
			else
				dbutton[i] = NONE;
			button[i] = cur_button;
		}
	}
}

float Gamepad::normalise(LONG raw_value, bool invert) {
	float pos;
	if (abs(raw_value - 32767) > 6000)
		pos = (raw_value - 32767) / 32768.0f;
	else
		pos = 0.0f;
	if (invert)
		pos = -pos;
	return pos;
};

BOOL CALLBACK Input::enumerate_gamepads(const DIDEVICEINSTANCE* device_instance, VOID* vec_gamepad_ptr) {
	vector<Gamepad>& gamepad = *static_cast<vector<Gamepad>*>(vec_gamepad_ptr);
	LPDIRECTINPUTDEVICE8 device;

	HRESULT hr;
	hr = Input::directinput->CreateDevice(device_instance->guidInstance, &device, NULL);

	if (hr == DI_OK)
		gamepad.push_back(device);

	return DIENUM_CONTINUE;
}

bool Input::refresh_devices(HWND hWnd) {
	for (auto& e : gamepad)
		e.release();
	gamepad.clear();
	HRESULT hr;
	directinput->EnumDevices(DI8DEVCLASS_GAMECTRL, enumerate_gamepads, &gamepad, DIEDFL_ATTACHEDONLY);
	for (size_t i = 0; i< gamepad.size(); i++) {
		hr = gamepad[i].device->SetDataFormat(&c_dfDIJoystick);
		if (hr != DI_OK)
			return false;
		hr = gamepad[i].device->SetCooperativeLevel(hWnd, DISCL_EXCLUSIVE | DISCL_BACKGROUND);
		if (hr != DI_OK)
			return false;
	}
	return true;
}

bool Input::init(HINSTANCE hinstance, HWND hWnd) {
	HRESULT hr;
	hr = DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&directinput, NULL);

	if (hr != DI_OK)
		return false;
	if (!refresh_devices(hWnd))
		return false;

	set_devicebroadcast(hWnd);

	return true;
}

void Input::shutdown() {
	directinput->Release();
	directinput = NULL;

	//std::for_each(gamepad.begin(), gamepad.end(), 
	for (auto& e : gamepad)
		e.release();

	gamepad.clear();
	reset_devicebroadcast();
}

void Input::set_devicebroadcast(HWND hwnd) {
	DEV_BROADCAST_DEVICEINTERFACE dev_interface;
	memset(&dev_interface, 0, sizeof(dev_interface));
	dev_interface.dbcc_size = sizeof(dev_interface);
	dev_interface.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	//HID GUID is {745a17a0-74d3-11d0-b6fe-00a0c90f57da}
	dev_interface.dbcc_classguid = { 0x745a17a0, 0x74d3, 0x11d0, { 0xb6, 0xfe, 0x00, 0xa0, 0xc9, 0x0f, 0x57, 0xda } };
	notifier = RegisterDeviceNotification(hwnd, &dev_interface, DEVICE_NOTIFY_WINDOW_HANDLE);
}

void Input::reset_devicebroadcast() {
	UnregisterDeviceNotification(notifier);
}