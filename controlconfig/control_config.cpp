// control_config.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "control_config.h"

#pragma comment(lib, "comctl32.lib")

//Newer style, Windows XPe compatible
#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='5.82.2900.2180' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
const int main_window_size_x = 800;
const int main_window_size_y = 400;
const static size_t num_buttons = 10;
const static std::wstring button_names[num_buttons] = { L"Left", L"Right", L"Up", L"Down", L"Start", L"Shot", L"Burst", L"C", L"Turn", L"Coin" };
const size_t num_players = 4;
const std::wstring player_names[num_players] = { L"Player 1", L"Player 2", L"Player 3", L"Player 4" };
HANDLE gui_font;

//;) -austere
bool debug_console = false;
int konami_buffer[16];
int konami_position = 0;
const int konami_code_size = 11;
const int konami_code[konami_code_size] = { VK_UP, VK_UP, VK_DOWN, VK_DOWN, VK_LEFT, VK_RIGHT, VK_LEFT, VK_RIGHT, 'B', 'A', VK_RETURN };

bool konami_check(int key) {
	konami_buffer[konami_position] = key;
	auto x = (konami_position + 16 - konami_code_size + 1) & 0xf;
	
	bool passed = true;
	for (int i = 0; i < konami_code_size && passed; i++)
		passed = (konami_code[i] == konami_buffer[(x + i) & 0xf]);
	konami_position = (konami_position + 1) & 0xf;
	return passed;
}

//TODO: Move to header
struct Window_assets {
	enum Button_Num { LEFT = 0, RIGHT, UP, DOWN, START, A, B, C, D, COIN };

	HWND button[num_buttons];
	const int button_height = 24;
	const int button_width = 100;
	const int button_y_align = 30;

	const int group_left_x = 20;
	const int group_left_y = 70;

	const int group_right_x = main_window_size_x - button_width - group_left_x;
	const int group_right_y = group_left_y;

	const int status_width = 500;
	const int status_height = 20;
	const int status_x = (main_window_size_x - status_width) / 2;
	const int status_y = main_window_size_y - 50;
	
	HWND status;

	const int label_height = 20;
	const int label_width = 240;
	const int label_y_align = button_y_align;
	const int label_x_align = 20;
	const int label_left_x = group_left_x + button_width + label_x_align;
	const int label_left_y = group_left_y + 2;
	const int label_right_x = group_right_x - label_x_align - label_width;
	const int label_right_y = group_left_y + 2;
	HWND label[num_buttons];

	//Combobox window size must fit it when its largest extent...
	const int combobox_height = 150;
	const int combobox_width = 200;
	const int combobox_x = (main_window_size_x - combobox_width) / 2;
	const int combobox_y = 20;
	HWND combobox;

	//Control ID constants
	const int IDC_BUTTON_BASE = 200;
	const int IDC_EDIT_BASE = IDC_BUTTON_BASE + num_buttons;
	const int IDC_LABEL_BASE = IDC_EDIT_BASE + num_buttons;
	const int IDC_STATUS = IDC_LABEL_BASE + 1;
	const int IDC_COMBOBOX = IDC_STATUS + 1;
};
Window_assets window;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
void CALLBACK populate(HWND hWnd);
void init(HWND hWnd);
void update_input();

namespace Ansi {
	HANDLE handle_stdout, handle_stdin;
	CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
	WORD old_attributes;
	WORD cur_attributes;

	bool init() {
		handle_stdout = GetStdHandle(STD_OUTPUT_HANDLE);
		if (handle_stdout == INVALID_HANDLE_VALUE) {
			return false;
		}

		if (!GetConsoleScreenBufferInfo(handle_stdout, &csbiInfo)) {
			return false;
		}
		old_attributes = csbiInfo.wAttributes;
		cur_attributes = old_attributes;
		return true;
	}

	void set_color(uint8_t color) {
		cur_attributes &= ~0xff;
		cur_attributes |= color;
		SetConsoleTextAttribute(handle_stdout, cur_attributes);
	}

	void reset() {
		SetConsoleTextAttribute(handle_stdout, old_attributes);
	}

	BOOL set_position(unsigned short x, unsigned short y) {
		COORD coord;
		coord.X = x;
		coord.Y = y;
		return SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	}

	BOOL set_position(COORD coord) {
		return SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	}

	COORD get_position() {
		CONSOLE_SCREEN_BUFFER_INFO info;
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &info);
		return info.dwCursorPosition;
	}
}

void CreateConsole(const unsigned int max_lines) {
	AllocConsole();

	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	HANDLE stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(stdHandle, &coninfo);
	coninfo.dwSize.Y = max_lines;
	SetConsoleScreenBufferSize(stdHandle, coninfo.dwSize);

	freopen("conout$", "w", stdout);
	setvbuf(stdout, NULL, _IONBF, 0);
	std::ios::sync_with_stdio();
}

void show_debug_console() {
	CreateConsole(200);
	printf("Debug console online\n");
	PlaySound(L"SystemAsterisk", NULL, SND_SYNC);
}

//Just for debugging
void console_display() {
	auto old_pos = Ansi::get_position();
	Ansi::set_position(0, 24 - Input::gamepad.size());
	int n = 1;
	for (auto& gamepad : Input::gamepad) {
		printf("%d: ", n++);
		if (gamepad.left_x < -0.5)
			printf("L");
		else
			printf("-");
		if (gamepad.left_x > 0.5)
			printf("R");
		else
			printf("-");
		if (gamepad.left_y > 0.5)
			printf("U");
		else
			printf("-");
		if (gamepad.left_y < -0.5)
			printf("D");
		else
			printf("-");

		int i = 0;
		for (auto& b : gamepad.button) {
			if (b)
				printf("%c", 'a' + i);
			else
				printf("-");
			++i;
		}
		puts("");
	}
	Ansi::set_position(old_pos);
}

//TODO: Move into window(), tighten initialisation process, delete copy etc.
namespace State {
	std::wstring status_text = L"Ready";
	char config_file[] = "config.ini";
	
	struct Key {
		std::string source_name;
		std::string key_name;
		//Unimplimented, key source from GUID
		GUID source_guid;

		Key(std::string source_name, std::string key_name) : source_name(source_name), key_name(key_name) {}

		bool operator== (const Key& x) {
			return (source_name == x.source_name) && (key_name == x.key_name);
		}
	};

	enum class Source { DINPUT = 0, XINPUT, KEYBOARD };
	const size_t num_players = 4;

	std::vector<std::string> source_names = { "D", "X", "K" };
	std::list<Key> keylist[num_players][num_buttons];
	std::map<std::string, int> button_map;

	bool touched = false;
	bool awaiting_input = false;
	int button_awaiting = -1;
	int current_player = 0;

	void button_selected(int number) {
		touched = true;
		button_awaiting = number;
		awaiting_input = true;
	}

	void input_received(Source a, int input_enumerator, std::string name) {
		if (!awaiting_input)
			return;
		awaiting_input = false;
		touched = true;

		std::string source_name = source_names[(size_t)a] + std::to_string(input_enumerator);
		auto new_key = Key(source_name, name);
		auto itr = std::find(keylist[current_player][button_awaiting].begin(), keylist[current_player][button_awaiting].end(), new_key);

		//Toggle key from list
		if (itr == keylist[current_player][button_awaiting].end()) {
			keylist[current_player][button_awaiting].push_back(new_key);
		}
		else {
			keylist[current_player][button_awaiting].erase(itr);
		}
	}

	void update() {
		if (awaiting_input) {
			status_text = L"Press input on controller to add/remove from button <" + button_names[button_awaiting] + L">'s keylist or hit ESC to cancel.";
		}
		else {
			status_text = L"Ready";
		}
	}

	std::string get_button_string(int button) {
		std::string key_list_string = "{";
		size_t i = 0;
		for (auto& key : keylist[current_player][button]) {
			if (i++)
				key_list_string += ",";
			key_list_string += key.source_name + "[" + key.key_name + "]";
		}
		key_list_string += "}";
		return key_list_string;
	}

	void update_window() {
		if (touched) {
			update();
			touched = false;
		}
		else
			return;

		SetWindowText(window.status, status_text.c_str());
		for (int button = 0; button < num_buttons; button++) {
			auto label_string = get_button_string(button);
			auto label_wstring = std::wstring(label_string.begin(), label_string.end());
			SetWindowText(window.label[button], label_wstring.c_str());
		}
	}

	void init() {
		touched = false;
		awaiting_input = false;
		button_awaiting = -1;
		for (size_t i = 0; i < num_buttons; i++) {
			auto name = std::string(button_names[i].begin(), button_names[i].end());
			button_map[name] = i;
		}
	}

	void canceled() {
		touched = true;
		awaiting_input = false;
		State::update_window();
		printf("Canceled\n");
	}

	void select_player(int player) {
		printf("Player %d selected.\n", player + 1);
		current_player = player;
		touched = true;
	}

	void update_input() {
		//DirectInput
		int n = 1;
		for (auto& gamepad : Input::gamepad) {
			if (gamepad.d_left_x < -0.5)
				input_received(Source::DINPUT, n, "L1");
			else if (gamepad.d_left_x > 0.5)
				input_received(Source::DINPUT, n, "R1");
			else if (gamepad.d_left_y < -0.5)
				input_received(Source::DINPUT, n, "D1");
			else if (gamepad.d_left_y > 0.5)
				input_received(Source::DINPUT, n, "U1");
			//TODO: set constant for size of Gamepad::dbutton
			for (size_t b = 0; b < 16; b++)
			if (gamepad.dbutton[b] && gamepad.dbutton[b])
				input_received(Source::DINPUT, n, "B" + std::to_string(b + 1));
			n++;
		}

		//XInput

		//Keyboard
	}

	bool add_from_string(int player, int button, std::string&& str) {
		printf("%s ", str.c_str());
		size_t source_type = 0;
		bool found = false;
		for (source_type = 0; !found && source_type < source_names.size(); source_type++) {
			if (str.compare(source_names[source_type])) {
				found = true;
				break;
			}
		}
		if (!found)
			return false;
		
		str.erase(0, source_names[source_type].size());
		size_t offset;
		int source_number = std::stoi(str, &offset, 10);
		if (str[offset] != '[' || str.back() != ']')
			return false;
		
		//Could use a bit of efficiency here lol. Fix it later.
		auto new_key = Key(source_names[source_type] + std::to_string(source_number), str.substr(offset + 1, str.size() - offset - 2));
		
		auto itr = std::find(keylist[current_player][button].begin(), keylist[current_player][button].end(), new_key);
		if (itr == keylist[current_player][button].end()) {
			keylist[current_player][button].push_back(new_key);
			return true;
		}
		return false;
	}

	bool load_config() {
		std::ifstream fs;
		fs.open(config_file, std::ios_base::in);
		if (!fs.is_open())
			return false;

		current_player = 0;
		auto stored_player = current_player;

		for (size_t j = 0; j < num_players; j++) {
			for (size_t i = 0; i < num_buttons; i++)
				keylist[j][i].clear();
		}

		auto skip_wspace = [](std::string& str, size_t offset) {
			for (; offset < str.size() && iswspace(str[offset]); offset++);
			return offset;
		};
		auto process_category = [&](std::string category) {
			printf("[%s]\n", category.c_str());
			try {
				auto player_number = category.substr(sizeof("Player") - 1);
				current_player = std::stoi(player_number) - 1;
			}
			catch (...) {
				current_player = 0;
			}
			if (current_player < 0 || current_player >= 4)
				current_player = 0;
		};
		auto process_item = [&](std::string item, std::string value) {
			printf("%s|=|%s ~ ", item.c_str(), value.c_str());
			auto itr = button_map.find(item);
			if (itr == button_map.end())
				return;
			auto num = itr->second;
			
			//Skips the '{', caller already removed whitespace
			size_t offset = 1;
			for (; offset < value.size();) {
				offset = skip_wspace(value, offset);
				if (offset >= value.size())
					break;
				size_t end_offset = offset;
				for (; end_offset < value.size() && (value[end_offset] != ',') && (value[end_offset] != '}'); end_offset++);
				if (offset != end_offset)
					add_from_string(current_player, num, value.substr(offset, end_offset - offset));
				for (offset = end_offset + 1; offset < value.size() && ((value[offset] == ',') || iswspace(value[offset])); offset++);
			}
			printf("\n");
		};

		std::string line;
		for (;;) {
			std::getline(fs, line);
			if (fs.eof())
				break;
			size_t offset = 0;
			offset = skip_wspace(line, offset);
			//Empty line or comment
			if (offset >= line.size() || line[offset] == ';')
				continue;
			
			//Category
			if (line[offset] == '[') {
				size_t end_offset = offset;
				for (; end_offset < line.size() && line[end_offset] != ']'; end_offset++);
				process_category(line.substr(offset + 1, end_offset - offset - 1));
			}
			//Item
			else {
				size_t end_offset = offset;
				for (; end_offset < line.size() && line[end_offset] != '=' && !iswspace(line[end_offset]); end_offset++);
				std::string item_name = line.substr(offset, end_offset - offset);
				offset = end_offset + 1;
				for (; offset < line.size() && (iswspace(line[offset]) || line[offset] == '='); offset++);
				std::string value;
				if (offset < line.size())
					value = line.substr(offset, std::string::npos);
				process_item(item_name, value);
			}
		}
		current_player = stored_player;
		touched = true;
		//update_window();
		return true;
	}

	bool save_config() {
		touched = true;
		std::string out;
		auto stored_player = current_player;
		for (size_t player = 0; player < num_players; player++) {
			current_player = player;
			if (player)
				out += "\n";
			out += "[Player" + std::to_string(player + 1) + "]\n";
			for (size_t i = 0; i < num_buttons; i++) {
				auto button_name = std::string(button_names[i].begin(), button_names[i].end());
				out += button_name + " = " + get_button_string(i) + "\n";
			}
		}
		current_player = stored_player;
		std::ofstream fs;
		fs.open(config_file, std::ios_base::out);
		if (!fs.is_open())
			return false;
		fs << out;
		bool is_bad = fs.bad();
		fs.close();
		return !is_bad;
	}
}

//TODO: Move into Window struct
void CALLBACK populate(HWND hWnd) {
	for (size_t i = 0; i < num_buttons; i++) {
		bool group_left = i < (num_buttons / 2);
		int x, y;
		int max_align_height = max((window.button_height + window.button_y_align), (window.label_height + window.label_y_align));
		if (group_left) {
			x = window.group_left_x;
			y = window.group_left_y + i * max_align_height;
		}
		else {
			x = window.group_right_x;
			y = window.group_right_y + (i - 5) * max_align_height;
		}
		auto tab_stop = !i ? WS_TABSTOP : 0;
		window.button[i] = CreateWindowEx(0, L"button", button_names[i].c_str(),
			tab_stop | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			x, y, window.button_width, window.button_height,
			hWnd, (HMENU)(window.IDC_BUTTON_BASE + i), hInst, NULL);

		int label_x, label_y;
		int prop = 0;
		if (group_left) {
			label_x = window.label_left_x;
			label_y = window.label_left_y + i * max_align_height;
			prop = ES_LEFT;
		}
		else {
			label_x = window.label_right_x;
			label_y = window.label_right_y + (i - 5) * max_align_height;
			prop = ES_RIGHT;
		}

		window.label[i] = CreateWindowEx(0, L"edit", L"",
			WS_DISABLED | WS_VISIBLE | WS_CHILD | ES_READONLY | ES_AUTOHSCROLL | prop,
			label_x, label_y, window.label_width, window.label_height,
			hWnd, NULL, hInst, NULL);
	}
	window.status = CreateWindowEx(0, L"edit", L"",
		WS_DISABLED | WS_VISIBLE | WS_CHILD | ES_READONLY | ES_AUTOHSCROLL | ES_CENTER,
		window.status_x, window.status_y, window.status_width, window.status_height,
		hWnd, (HMENU)(window.IDC_EDIT_BASE), hInst, NULL);

	window.combobox = CreateWindowEx(0, L"combobox", L"",
		CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
		window.combobox_x, window.combobox_y, window.combobox_width, window.combobox_height,
		hWnd, NULL, hInst, NULL);

	for (size_t i = 0; i < num_players; i++)
		SendMessage(window.combobox, CB_ADDSTRING, 0, (LPARAM)player_names[i].c_str());
	SendMessage(window.combobox, CB_SETCURSEL, 0, 0);
	SendMessage(window.combobox, CB_SETMINVISIBLE, 4, 0);

	//Nicer font than System *vomit*, leaves more room description
	gui_font = GetStockObject(DEFAULT_GUI_FONT);
	EnumChildWindows(hWnd, [](HWND child, LPARAM lParam) -> BOOL {
		SendMessage(child, WM_SETFONT, (WPARAM)lParam, TRUE);
		return TRUE;
	}, (LPARAM)gui_font);
}

//TODO: Move into window struct
void init(HWND hWnd) {
	State::init();
	Input::init(hInst, hWnd);
	size_t num_gamepads = Input::gamepad.size();
	printf("%d gamepad%s detected\n", num_gamepads, (num_gamepads == 1) ? "" : "s");
	State::touched = true;
	State::load_config();
	State::update_window();
}

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	if (debug_console)
		show_debug_console();

	//Use new common controls
	INITCOMMONCONTROLSEX comm_controls;
	comm_controls.dwSize = sizeof(comm_controls);
	comm_controls.dwICC = ICC_LISTVIEW_CLASSES;
	InitCommonControlsEx(&comm_controls);

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_CONTROL_CONFIG, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	Timer::set_resolution(1);

	int frame_count = 0;
	Frame_waiter frame_waiter(60);
	frame_waiter.start();
	DWORD prev_count = GetTickCount();
	
	MSG msg;
	HACCEL hAccelTable;
	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_CONTROL_CONFIG));
	bool state_quit = false;

	// Main message loop:
	while (!state_quit) {
		for (; PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);) {
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			if (msg.message == WM_QUIT) {
				state_quit = true;
				break;
			}
		}

		if (frame_waiter.wait_partial()) {
			//Frame rate calculation
			frame_count++;
			if (frame_count % frame_waiter.frame_rate == 0) {
				DWORD ticks = GetTickCount() - prev_count;
				float fps = 1000 * frame_waiter.frame_rate / (float)ticks;
				//printf("FPS: %f\n", fps);
				prev_count = GetTickCount();
			}

			for (auto& gamepad : Input::gamepad) {
				gamepad.update();
			}
			State::update_input();
			State::update_window();
		}
	}

	Input::shutdown();
	State::save_config();
	return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_CONTROL_CONFIG));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOWFRAME);
	wcex.lpszMenuName	= NULL; // MAKEINTRESOURCE(IDC_CONTROL_CONFIG);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
//Modified for generic resolution, removed borders from calculation, i.e
//main_window_size_x/y are the borderless resolution
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	auto dwStyle = WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME;
	auto dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;

	//Windows have borders and these change in dimensions depending on the version of windows
	//This calculates the required dimensions to be passed on to CreateWindowEx!
	RECT rect = { 0, 0, main_window_size_x, main_window_size_y };
	AdjustWindowRectEx(&rect, dwStyle, false, dwExStyle);
	int window_width = rect.right - rect.left;
	int window_height = rect.bottom - rect.top;
	
	//Disable resizing
	hWnd = CreateWindowEx(dwExStyle, szWindowClass, szTitle, dwStyle,
		CW_USEDEFAULT, 0, window_width, window_height, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}
	
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

void device_changed(HWND hWnd, WPARAM wParam, LPARAM lParam) {
	switch (wParam) {
	case DBT_DEVNODES_CHANGED:
		{
			Input::refresh_devices(hWnd);
			size_t num_gamepads = Input::gamepad.size();
			printf("%d gamepad%s detected\n", num_gamepads, (num_gamepads == 1) ? "" : "s");
		}
		break;
	default:
		break;
	}
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);

		if (wmId >= window.IDC_BUTTON_BASE && wmId < (int)(window.IDC_BUTTON_BASE + num_buttons)) {
			int button_number = wmId - window.IDC_BUTTON_BASE;
			State::button_selected(button_number);
			//Drops focus after  setting in order to process WM_KEYDOWN messages
			SetFocus(hWnd);
		}

		if (wmEvent == CBN_SELCHANGE) {
			int item = SendMessage((HWND)lParam, CB_GETCURSEL, 0, 0);
			State::select_player(item);
		}

		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_CREATE:
		populate(hWnd);
		init(hWnd);
		break;
	case WM_KEYDOWN:
		if (wParam == VK_ESCAPE)
			State::canceled();
		if (konami_check(wParam)) {
			if (!debug_console) {
				show_debug_console();
			}
			else
				FreeConsole();
			debug_console = !debug_console;
		}
		break;
	case WM_DEVICECHANGE:
		device_changed(hWnd, wParam, lParam);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}